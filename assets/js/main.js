
$(document).ready(function() {
    $(".toggle-password").click(function() {
        console.log($(this));
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        console.log(input);
        if (input.attr("type") == "password") {
          input.attr("type", "text");
        } else {
          input.attr("type", "password");
        }
    });

    $("#basic-form").validate({
      rules: {
        fullname: {
            required: true,
        },
        email: {
          required: true,
          email: true
        },
        password: {
            minlength: 5,
            required: {
                depends: function(elem) {
                    if (
                    $("#password").val().match( /[0-9]/g) && 
                    $("#password").val().match( /[A-Z]/g) && 
                    $("#password").val().match(/[a-z]/g) && 
                    $("#password").val().match( /[^a-zA-Z\d]/g) &&
                    $("#password").val().length >= 10
                    ) {
                        return true
                    }
                    else{
                        return false;
                    }
                }
              },
        },
        password_confirm: {
            minlength: 5,
            equalTo: "#password"
        }
      },
      messages : {
        fullname: {
          required: "Champ Obligatoire2",
        },
        email: {
          email: "Veuillez respecter ce format please abc@domain.tld"
        },
        password: {
            required: "Champ Obligatoire1",
            minlength: "Champ petit",

        },
        // password_confirm: {
        //     equalTo : "Non conforme"
        // }
      }
    });
    console.log($("#basic-form"));
});